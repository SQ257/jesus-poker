# Poker
### Getting Started
requirements: node / npm

* run `npm install` - to pull down dependencies
* run `npm start` - to spin up local development enviroment using webpack-dev-server
* point browser to `localhost:3000`

#### other options
run `npm run build` to bundle into a `dist/` directory for quick & easy viewing / video poker-ing


This video poker application is written using [React.js](https://reactjs.org/) and is leveraging the [Redux Library](https://redux.js.org/) to manage its state.
