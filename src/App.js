import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as pokerActions from 'redux/actions'
import Deck from 'deck';
import Scoring from 'scoring';


import ButtonComponent from 'ButtonComponent';
import HandComponent from 'HandComponent';

import './styles/base.scss';


class App extends Component {
  componentDidMount(){
    this.props.dispatch(pokerActions.createNewDeck());
  }

  
  handleActionClick = () => {
    const { poker } = this.props;

    switch (poker.status) {
      case 'ready':

        this.props.dispatch(
          pokerActions.dealCards(
            Deck.deal(poker.deck, [])
          )
        );

        break;
      case 'waiting':

        this.props.dispatch(
          pokerActions.dealCards(
            Deck.deal(poker.deck,poker.hand)
          )
        );

        this.props.dispatch(
          pokerActions.scoreGame(
            Scoring.score(poker.hand)
          )
        );

        this.props.dispatch(pokerActions.createNewDeck());



        break;
      default:
        console.error('state broken');
    }
  }

  render() {

    return (
      <div>
        <h1>Poker</h1>
        <span className='score'>score: {this.props.poker.score}</span>
        <HandComponent />
        <ButtonComponent click={this.handleActionClick} />
        <span className='status'>Dealer is: {this.props.poker.status}</span>
      </div>
    );

  }
}


function mapStateToProps(state) {
  return {
    poker: state
  };
}

export default connect(mapStateToProps)(App);
