import React, { Component } from 'react';
import { connect } from 'react-redux';

import CardComponent from 'CardComponent';

class HandComponent extends Component {
  constructor (props) {
      super(props);

      this.state = {
        visible: false,
        disabled: true
      }
  }
  buildHand = () => this.props.poker.hand.map((card,i) => {
    return ( <CardComponent card={card} index={i} key={i} /> );
  });

  componentWillReceiveProps(nextProps){
      if(nextProps.poker.hand.length){
        this.setState({
          visible: true
        });

        if(nextProps.poker.status === 'ready'){
          this.setState({
            disabled: true
          });
        } else {
          this.setState({
            disabled: false
          });
        }
      }
  }

  render() {
    return (
      <div  className={`hand-container visible-${this.state.visible} disabled-${this.state.disabled}`}>
        <ul>
          {this.props.poker.hand && this.buildHand()}
        </ul>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    poker: state
  };
}

export default connect(mapStateToProps)(HandComponent);
