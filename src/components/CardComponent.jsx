import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as pokerActions from 'redux/actions'

class CardComponent extends Component {

  handleClick = () => {
    if(this.props.poker.status !== 'waiting'){
      return false;
    }
    const {card} = this.props;
    let markedStatus = card.marked;
    let updatedHand = this.props.poker.hand;

    //take this card and toggle its status
    //updates the entire hand in the state
    updatedHand[this.props.index].marked = !markedStatus;

    this.props.dispatch(
      pokerActions.updateHand(updatedHand)
    )
  }

  render() {
    const {card} = this.props;
    return (
      <li className={`marked-${card.marked} ${card.suit} card`} onClick={this.handleClick}>{card.name}</li>
    );
  }
}

function mapStateToProps(state) {
  return {
    poker: state
  };
}


export default connect(mapStateToProps)(CardComponent);
