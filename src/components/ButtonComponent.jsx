import React, { Component } from 'react';
import { connect } from 'react-redux';



class ButtonComponent extends Component {

  render() {
    return (
      <button onClick={this.props.click}>{this.props.poker.buttonLabel}</button>
    );
  }
}


function mapStateToProps(state) {
  return {
    poker: state
  };
}

export default connect(mapStateToProps)(ButtonComponent);
