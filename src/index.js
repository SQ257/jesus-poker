import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createLogger } from 'redux-logger';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import reducer from './redux/reducer';

const store = createStore(
  reducer,
  applyMiddleware(createLogger())
);




ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
