
/**
 * newDeck - retruns a newly created and shuffled deck
 */
export const newDeck = () => {
  let deck = [];

  const names = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
  const suits = ['hearts','diamonds','spades','clubs'];

  for( let s = 0; s < suits.length; s++ ) {
      for( let n = 0; n < names.length; n++ ) {
          deck.push( new card( n+1, names[n], suits[s] ) );
      }
  }


  return shuffle(deck);
};

/**
 * takes the current deck and hand arrays
 * and moves cards from the deck to the hand as necessary
 * based on the handMax value
 */
export const deal = (deck,hand) =>{
  const numberOfMarkedCards = countMarkedCards(hand);
  const thisHand = hand;

  const handMax = 5;
  const cardsToDeal = handMax - thisHand.length + numberOfMarkedCards;

  if(thisHand.length < handMax){
    for (let i = 0; i < cardsToDeal; i++) {
      thisHand.push(deck[i]);
    }
  } else {
    let deckIndex = 0;

    for (let j = 0; j < thisHand.length; j++) {
      if(thisHand[j].marked){
        thisHand.splice(j,1, deck[deckIndex]);
        deckIndex++;
      }
    }
  }

  //remove cards being delt from deck
  deck.splice(0,cardsToDeal);

  return thisHand;

}


// private
const countMarkedCards = (hand) => {
  let count = 0;
  for (var i = 0; i < hand.length; i++) {
    if(hand[i].marked === true){
      count++
    }
  }
  return count;
}

/**
 * defines properties of a card
 */
const card = (value,name,suit) => {

  return {
    value,
    name,
    suit,
    marked: false
  }

}

/**
 * takes an array and shuffles it
 * by selecting an element & swapping it out with a new random item
 */
const shuffle = (array) => {
  let currentIndex = array.length,
        temporaryValue,
        randomIndex;

    while (0 !== currentIndex) {


      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
}



export default {
  newDeck,
  deal
};
