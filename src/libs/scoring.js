const points = {
  STRAIGHT: 500,
  PAIR: 200
}

export const score = (hand) => {
  if(checkIfStraightHand(hand)){
    return points.STRAIGHT;
  } else if(checkIfPair(hand)){
    return points.PAIR;
  } else {
    return 0;
  }

};


/**
 * Takes the hand and loops through its values in a temporary
 * "values" array to determine if there is a pair.
 */
const checkIfPair = (hand) => {
  let values = [];

  for (var i = 0; i < hand.length; i++) {
    values.push(hand[i].value)
  }
  return new Set(values).size !== values.length;
}


const checkIfStraightHand = (hand) => {
  let lastCard = null;
  let tempValues = []


  for (var i = 0; i < hand.length; i++) {
    tempValues.push(hand[i].value)
  }

  tempValues.sort(function(a, b){return a-b});

  for (var j = 0; j < tempValues.length; j++) {
    if(lastCard === null || lastCard + 1 === tempValues[j]){
      lastCard = tempValues[j];
    } else {
      return false;
    }
  }

  return true;
}

export default {
  score
};
