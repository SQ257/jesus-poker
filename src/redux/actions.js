import Deck from 'deck';

export const createNewDeck = () =>{
  return {
    type: 'CREATE_NEW_DECK',
    deck: Deck.newDeck()
  }
}


export const dealCards = (hand) =>{
  return {
    type: 'DEAL_CARDS',
    hand: hand
  }
}


export const updateHand = (hand) =>{
  return {
    type: 'UPDATE_HAND',
    hand: hand
  }
}

export const scoreGame = (score) =>{
  return {
    type: 'UPDATE_SCORE',
    score: score
  }
}

//
// export const flipTable = () =>{
//   return {
//     type: 'FLIP_TABLE'
//   }
// }
