
const initialState = {
  deck: [],
  hand: [],
  buttonLabel: 'Deal',
  status: 'ready',
  score: 0
};


export default function reducer(state = initialState, action = {}) {
  switch(action.type) {
    case 'CREATE_NEW_DECK':
      return {
        ...state,
        deck: action.deck,
        status: 'ready'
      };
    case 'UPDATE_SCORE':
      return {
        ...state,
        score: state.score + action.score,
        buttonLabel: initialState.buttonLabel
      };
    case 'UPDATE_HAND':
      return {
        ...state,
        hand: action.hand
      };
    case 'DEAL_CARDS':
      return {
        ...state,
        status: 'waiting',
        buttonLabel: 'Go',
        hand: action.hand
      }
    default:
      return state;
  }
}
